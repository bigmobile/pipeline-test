#Installing nodejs and npm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install node

#Staging
if [ "$DEPLOYMENT_GROUP_NAME" == "development" ]
then
    rsync -av /test/www/ /test/cd
    cd /test/cd
    npm install

    rm -rf /test/cd/build
    REACT_APP_ENV=development npm run build
fi

#Production
if [ "$DEPLOYMENT_GROUP_NAME" == "production" ]
then
    rsync -av /test/www/ /test/html
    cd /front/html
    npm install

    rm -rf /front/html/build
    REACT_APP_ENV=production npm run build
fi
